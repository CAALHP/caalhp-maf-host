﻿using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.AddInViews;

namespace Plugins.DriverPluginAdapters
{
    abstract public class DeviceDriverViewPluginAdapter : IDeviceDriverView
    {
        //public HostViewToCAALHPContractAdapter Host { get; set; }
        public IDeviceDriverCAALHPContract DeviceDriver { get; set; }

        /*public double GetMeasurement()
        {
            return DeviceDriver.GetMeasurement();
        }*/

        public string GetName()
        {
            return DeviceDriver.GetName();
        }

        public void Initialize(IDeviceDriverHostView hostView, int processId)
        {
            var host = new DeviceDriverHostViewToCAALHPContractAdapter(hostView);
            DeviceDriver.Initialize(host, processId);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            DeviceDriver.Notify(notification);
        }

        public bool IsAlive()
        {
            return DeviceDriver.IsAlive();
        }

        public void ShutDown()
        {
            DeviceDriver.ShutDown();
        }
    }
}
