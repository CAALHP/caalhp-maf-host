﻿using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.AddInViews;

namespace Plugins.ServicePluginAdapter
{
    abstract public class ServiceViewPluginAdapter : IServiceView
    {
        //public HostViewToCAALHPContractAdapter Host { get; set; }
        public IServiceCAALHPContract Service { get; set; }

        public string GetName()
        {
            return Service.GetName();
        }

        public void Initialize(IServiceHostView hostView, int processId)
        {
            var host = new ServiceHostViewToCAALHPContractAdapter(hostView);
            Service.Initialize(host, processId);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            Service.Notify(notification);
        }

        public bool IsAlive()
        {
            return Service.IsAlive();
        }

        public void ShutDown()
        {
            Service.ShutDown();
        }

        public void Start()
        {
            Service.Start();
        }

        public void Stop()
        {
            Service.Stop();
        }
    }
}
