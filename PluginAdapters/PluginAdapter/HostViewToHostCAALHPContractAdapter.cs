using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.AddInViews;

namespace Plugins.PluginAdapter
{
    public class HostViewToHostCAALHPContractAdapter : IHostCAALHPContract
    {
        private readonly IHostView _hostView;

        public HostViewToHostCAALHPContractAdapter(IHostView host)
        {
            _hostView = host;
        }

        public void RegisterPlugin(string name, string category)
        {
            //null implementation
        }

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            _hostView.ReportEvent(value);
        }
        /*
        public void ReportEvent(string fullyQualifiedNameSpace, KeyValuePair<string, string> value)
        {
            _hostView.ReportEvent(fullyQualifiedNameSpace, value);
        }*/
        /*
        public void SubscribeToEvents(int processId)
        {
            _hostView.SubscribeToEvents(processId);
        }*/

        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _hostView.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
        /*
        public void UnSubscribeToEvents(int processId)
        {
            _hostView.UnSubscribeToEvents(processId);
        }*/

        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _hostView.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}