﻿using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.AddInViews;
using Plugins.PluginAdapter;

namespace Plugins.AppPluginAdapters
{
    public class AppHostViewToCAALHPContractAdapter : IAppHostCAALHPContract
    {
        private readonly IAppHostView _hostView;
        public IHostCAALHPContract Host { get; set; }

        public AppHostViewToCAALHPContractAdapter(IAppHostView host)
        {
            _hostView = host;
            Host = new HostViewToHostCAALHPContractAdapter(_hostView.Host);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _hostView.GetListOfInstalledApps();
        }

        public void ShowApp(string appName)
        {
            _hostView.ShowApp(appName);
        }

        public void CloseApp(string appName)
        {
            _hostView.CloseApp(appName);
        }

    }
}