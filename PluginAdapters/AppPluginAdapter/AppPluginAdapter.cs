﻿using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.AddInViews;

namespace Plugins.AppPluginAdapters
{
    public class AppPluginAdapter : IAppView
    {
        public IAppCAALHPContract App { get ; set; }

        public void Notify(KeyValuePair<string, string> notification)
        {
            App.Notify(notification);
        }

        public bool IsAlive()
        {
            return App.IsAlive();
        }

        public void ShutDown()
        {
            App.ShutDown();
        }

        public void Show()
        {
            App.Show();
        }

        public string GetName()
        {
            return App.GetName();
        }

        public void Initialize(IAppHostView hostView, int processId)
        {
            var host = new AppHostViewToCAALHPContractAdapter(hostView);
            App.Initialize(host, processId);
        }
    }
}
