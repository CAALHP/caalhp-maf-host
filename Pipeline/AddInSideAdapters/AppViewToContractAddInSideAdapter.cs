﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.AddInViews;
using Plugins.Contracts;

namespace Plugins.AddInSideAdapters
{
    // The AddInAdapterAttribute identifes this class as the add-in-side adapter 
    // pipeline segment.
    [AddInAdapter()]
    public class AppViewToContractAddInSideAdapter :
        ContractBase, IAppContract
    {
        private readonly IAppView _view;

        public AppViewToContractAddInSideAdapter(IAppView view)
        {
            _view = view;
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            _view.Notify(notification);
        }

        public bool IsAlive()
        {
            return _view.IsAlive();
        }

        public void ShutDown()
        {
            _view.ShutDown();
        }

        public void Show()
        {
            _view.Show();
        }

        public string GetName()
        {
            return _view.GetName();
        }

        public void Initialize(IAppHostContract hostObj, int processId)
        {
            _view.Initialize(new AppHostObjectContractToViewAddInAdapter(hostObj), processId);
        }
    }
}