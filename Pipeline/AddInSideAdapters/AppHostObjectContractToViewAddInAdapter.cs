﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.AddInViews;
using Plugins.Contracts;

namespace Plugins.AddInSideAdapters
{
    [AddInAdapter()]
    public class AppHostObjectContractToViewAddInAdapter : IAppHostView
    {
        private readonly IAppHostContract _contract;
        public IHostView Host { get; set; }

        public AppHostObjectContractToViewAddInAdapter(IAppHostContract contract)
        {
            _contract = contract;
            Host = new HostObjectContractToViewAddInAdapter(_contract.Host);
        }
        
        public void ShowApp(string appName)
        {
            _contract.ShowApp(appName);
        }

        public void CloseApp(string appName)
        {
            _contract.CloseApp(appName);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _contract.GetListOfInstalledApps();
        }
    }
}