﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.AddInViews;
using Plugins.Contracts;

namespace Plugins.AddInSideAdapters
{
    // The AddInAdapterAttribute identifes this class as the add-in-side adapter 
    // pipeline segment.
    [AddInAdapter()]
    public class ServiceViewToContractAddInSideAdapter :
        ContractBase, IServiceContract
    {
        private readonly IServiceView _view;

        public ServiceViewToContractAddInSideAdapter(IServiceView view)
        {
            _view = view;
        }

        public string GetName()
        {
            return _view.GetName();
        }

        public void Initialize(IServiceHostContract hostObj, int processId)
        {
            _view.Initialize(new ServiceHostObjectContractToViewAddInAdapter(hostObj), processId);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            _view.Notify(notification);
        }

        public bool IsAlive()
        {
            return _view.IsAlive();
        }

        public void ShutDown()
        {
            _view.ShutDown();
        }

        public void Start()
        {
            _view.Start();
        }

        public void Stop()
        {
            _view.Stop();
        }

    }
}