﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.AddInViews;
using Plugins.Contracts;

namespace Plugins.AddInSideAdapters
{
    [AddInAdapter()]
    public class HostObjectContractToViewAddInAdapter : IHostView
    {
        private readonly IHostContract _contract;
        private ContractHandle _handle;

        public HostObjectContractToViewAddInAdapter(IHostContract contract)
        {
            _contract = contract;
            _handle = new ContractHandle(_contract);
        }

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            _contract.ReportEvent(value);
        }

        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _contract.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
        
        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _contract.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}