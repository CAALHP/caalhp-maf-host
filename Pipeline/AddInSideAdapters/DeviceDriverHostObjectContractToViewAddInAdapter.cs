﻿using System.AddIn.Pipeline;
using Plugins.AddInViews;
using Plugins.Contracts;

namespace Plugins.AddInSideAdapters
{
    [AddInAdapter()]
    public class DeviceDriverHostObjectContractToViewAddInAdapter : IDeviceDriverHostView
    {
        private readonly IDeviceDriverHostContract _contract;

        public IHostView Host { get; set; }

        public DeviceDriverHostObjectContractToViewAddInAdapter(IDeviceDriverHostContract contract)
        {
            _contract = contract;
            Host = new HostObjectContractToViewAddInAdapter(_contract.Host);
        }
    }
}