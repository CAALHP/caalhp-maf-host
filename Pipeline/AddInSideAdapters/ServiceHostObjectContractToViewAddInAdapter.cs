﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.AddInViews;
using Plugins.Contracts;

namespace Plugins.AddInSideAdapters
{
    [AddInAdapter()]
    public class ServiceHostObjectContractToViewAddInAdapter : IServiceHostView
    {
        private readonly IServiceHostContract _contract;
        public IHostView Host { get; set; }
        
        public ServiceHostObjectContractToViewAddInAdapter(IServiceHostContract contract)
        {
            _contract = contract;
            Host = new HostObjectContractToViewAddInAdapter(_contract.Host);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _contract.GetListOfInstalledApps();
        }

        public IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            return _contract.GetListOfInstalledDeviceDrivers();
        }

        public void CloseApp(string fileName)
        {
            _contract.CloseApp(fileName);
        }

        public void ActivateDeviceDrivers()
        {
            _contract.ActivateDeviceDrivers();
        }

        public IList<string> GetListOfEventTypes()
        {
            return _contract.GetListOfEventTypes();
        }
    }
}