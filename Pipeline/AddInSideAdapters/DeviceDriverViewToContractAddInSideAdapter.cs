﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.AddInViews;
using Plugins.Contracts;

namespace Plugins.AddInSideAdapters
{
    // The AddInAdapterAttribute identifes this class as the add-in-side adapter 
    // pipeline segment.
    [AddInAdapter()]
    public class DeviceDriverViewToContractAddInSideAdapter :
        ContractBase, IDeviceDriverContract
    {
        private readonly IDeviceDriverView _view;

        public DeviceDriverViewToContractAddInSideAdapter(IDeviceDriverView view)
        {
            _view = view;
        }

        /*public double GetMeasurement()
        {
            return _view.GetMeasurement();
        }*/

        public string GetName()
        {
            return _view.GetName();
        }

        public void Initialize(IDeviceDriverHostContract hostObj, int processId)
        {
            _view.Initialize(new DeviceDriverHostObjectContractToViewAddInAdapter(hostObj), processId);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            _view.Notify(notification);
        }

        public bool IsAlive()
        {
            return _view.IsAlive();
        }

        public void ShutDown()
        {
            _view.ShutDown();
        }
    }
}