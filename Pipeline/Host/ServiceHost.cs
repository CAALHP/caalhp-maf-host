﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CAALHP.Library.Brokers;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Observer;
using CAALHP.Library.Visitor;
using Plugins.Host.HostViews;
using Plugins.Host.LifeCycleManagers;
using Plugins.Host.Processes;
using Plugins.HostViewAdapters;

namespace Plugins.Host
{
    public class ServiceHost : Host, IServiceHost
    {
        //private readonly Dictionary<int, ServiceProcess> _serviceDictionary = new Dictionary<int, ServiceProcess>();
        private int _serviceDictionaryIndex;
        public ServiceLifeCycleManager LifeCycleManager { get; private set; }

        protected ServiceHostView HostView { get; private set; }

        private readonly ICAALHPBroker _broker;

        public void ActivateDrivers()
        {
            _broker.ActivateDeviceDrivers();
        }

        public IList<string> GetListOfEventTypes()
        {
            return EventManager.EventSubject.NameSpaces;
        }

        public ServiceHost(EventManager eventManager, ICAALHPBroker broker)
            : base(eventManager)
        {
            _broker = broker;
            HostView = new ServiceHostView(this);
            LifeCycleManager = new ServiceLifeCycleManager(AddInRoot, HostView);
            ActivateServices();
            StartServices();
        }

        ~ServiceHost()
        {
            StopServices();
        }

        private async void ActivateServices()
        {
            LifeCycleManager.StartPlugins();
            //await Task.Run(() =>
            //{
            //    UpdateAddInStore();
            //    var tokens = AddInStore.FindAddIns(typeof(IServiceView), AddInRoot);
            //    foreach (var token in tokens)
            //    {
            //        ActivateService(token);
            //    }
            //});
        }

        //private async void ActivateService(AddInToken token)
        //{
        //    if (!TokenIsInDictionary(token))
        //    {
        //        await ActivateAsync(token);
        //    }
        //}

        //private async Task ActivateAsync(AddInToken token)
        //{
        //    await Task.Run(() =>
        //    {
        //        var service = token.Activate<IServiceView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
        //        //var Service = token.Activate<IServiceView>(AddInSecurityLevel.Internet);
        //        var installedServices = GetInstalledPlugins();
        //        var config = installedServices.FirstOrDefault(x => x.Config.PluginName.Equals(token.Name));
        //        var serviceProcess = new ServiceProcess(service, config);
        //        service.Initialize(HostView, serviceProcess.ProcessId);
        //        LifeCycleManager.ServiceDictionary.Add(_serviceDictionaryIndex++, serviceProcess);
        //        Console.WriteLine("activated Service: " + token.Name);
        //    });
        //}

        private IList<PluginConfig> GetInstalledPlugins()
        {
            var plugins = new List<PluginConfig>();
            var folders = Directory.EnumerateDirectories(Path.Combine(AddInRoot, "AddIns"));
            foreach (var folder in folders)
            {
                foreach (var file in Directory.GetFiles(folder))
                {
                    var fileName = Path.GetFileName(file);
                    if (fileName == null || !fileName.Equals("CareStorePlugin.xml")) continue;
                    var config = PluginConfigurationManager.GetConfig(file);
                    var plugin = new PluginConfig { Config = config, Directory = Path.GetDirectoryName(file) };
                    plugins.Add(plugin);
                    /*foreach (var runfile in config.RunFiles)
                        {
                            var plugin = new PluginInfo();
                            plugin.LocationDir = Path.GetDirectoryName(file);
                            plugin.Name = runfile;
                            plugins.Add(plugin);
                        }*/
                }
            }
            return plugins;
        }

        private void StartServices()
        {
            foreach (var serviceProcess in LifeCycleManager.ServiceDictionary)
            {
                serviceProcess.Value.Service.Start();
            }
        }

        private void StopServices()
        {
            /*foreach (var serviceProcess in _serviceDictionary)
            {
                serviceProcess.Value.Service.Stop();
            }*/
        }

        //private bool TokenIsInDictionary(AddInToken token)
        //{
        //    if (token == null) throw new ArgumentNullException("token");
        //    return LifeCycleManager.ServiceDictionary.Values.Any(service => AddInController.GetAddInController(service.Service).Token.Equals(token));
        //}

        public override void RegisterPlugin(string name, string category)
        {
            //null implementation
        }

        public override IList<PluginConfig> GetListOfInstalledApps()
        {
            return _broker != null ? _broker.GetListOfInstalledApps() : null;
        }

        public override IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            return _broker != null ? _broker.GetListOfInstalledDeviceDrivers() : null;
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.ServiceDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.ServiceDictionary[processId];
            EventManager.EventSubject.Detach(plugin, fullyQualifiedNameSpace);
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.ServiceDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.ServiceDictionary[processId];
            EventManager.EventSubject.Attach(plugin, fullyQualifiedNameSpace);
        }

        public override void ShutDown()
        {
            LifeCycleManager.StopPlugins();
        }

        public override void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void ShowApp(string appName)
        {
            throw new NotImplementedException();
        }

        public override void CloseApp(string appName)
        {
            if (_broker != null)
            {
                _broker.CloseApp(appName);
            }
        }


    }
}
