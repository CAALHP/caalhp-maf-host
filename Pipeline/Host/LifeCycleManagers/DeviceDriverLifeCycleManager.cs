﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using CAALHP.Library.Config;
using Plugins.Host.Processes;
using Plugins.HostViewAdapters;

namespace Plugins.Host.LifeCycleManagers
{
    public class DeviceDriverLifeCycleManager : LifeCycleManager
    {
        private readonly IDeviceDriverHostView _hostView;
        public Dictionary<int, DriverProcess> DeviceDriverDictionary { get; private set; }

        public DeviceDriverLifeCycleManager(string pluginRoot, IDeviceDriverHostView hostView)
            : base(pluginRoot)
        {
            _hostView = hostView;
            DeviceDriverDictionary = new Dictionary<int, DriverProcess>();
        }

        public override void StopPlugins()
        {
            KeepAliveTimer.Stop();
            ShouldKeepAlive = false;
            foreach (
                var deviceDriverProcess in
                    DeviceDriverDictionary.Values.Where(deviceDriverProcess => deviceDriverProcess.DeviceDriver != null)
                )
            {
                //We expect communication to break after this call, so we will not wait for a response.
                var process = deviceDriverProcess;
                Task.Run(() =>
                {
                    try
                    {
                        process.DeviceDriver.ShutDown();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Plugin already shut down");
                    }
                });
            }
        }

        protected override int GetRestartCounter(int processId)
        {
            return DeviceDriverDictionary.ContainsKey(processId) ? DeviceDriverDictionary[processId].RestartCount : int.MaxValue;
        }

        protected override void IncrementRestartCounter(int processId)
        {
            if (DeviceDriverDictionary.ContainsKey(processId)) DeviceDriverDictionary[processId].RestartCount++;
        }

        protected override void ReactivatePlugin(int key)
        {
            UpdateAddInStore();
            var tokens = AddInStore.FindAddIns(typeof(IDeviceDriverView), PluginRoot);
            var pluginName = DeviceDriverDictionary[key].Config.Config.PluginName;
            foreach (var token in tokens.Where(token => token.Name.Equals(pluginName)))
            {
                DeviceDriverDictionary[key].DeviceDriver = token.Activate<IDeviceDriverView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
                var newProcessId =
                    AddInController.GetAddInController(DeviceDriverDictionary[key].DeviceDriver).AddInEnvironment.Process.ProcessId;

                DeviceDriverDictionary[key].DeviceDriver.Initialize(_hostView, newProcessId);
                DeviceDriverDictionary.Add(newProcessId, DeviceDriverDictionary[key]);
                DeviceDriverDictionary.Remove(key);
                Console.WriteLine("reactivated device driver: " + token.Name);
                //DeviceDriverDictionary[key].DeviceDriver.Show();
            }
        }

        protected override void DeactivatePlugin(int processId)
        {
            try
            {
                if (!DeviceDriverDictionary.ContainsKey(processId)) return;
                var view = DeviceDriverDictionary[processId].DeviceDriver;
                if (view == null) return;
                DeviceDriverDictionary[processId].DeviceDriver = null;
                var controller = AddInController.GetAddInController(view);
                controller.Shutdown();
            }
            catch (Exception e)
            {
                Console.WriteLine("plugin was already closed");
            }
        }

        protected override void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var deviceDriverProcess in DeviceDriverDictionary)
            {
                var process = deviceDriverProcess;
                Task.Run(() =>
                {
                    if (process.Value.DeviceDriver == null) return;
                    try
                    {
                        if (!process.Value.DeviceDriver.IsAlive())
                        {
                            RestartPlugin(process.Key);
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine(e);
                        RestartPlugin(process.Key);
                    }
                });
            }
        }

        public override async Task ActivatePlugin(PluginConfig plugin)
        {
            await Task.Run(() =>
            {
                if (PluginIsActive(plugin)) return;
                UpdateAddInStore();
                var tokens = AddInStore.FindAddIns(typeof(IDeviceDriverView), PluginRoot);
                //var app = tokens[appIndex].Activate<IApp>(AddInSecurityLevel.Internet);
                //activate in a new process
                //var app = tokens[appIndex].Activate<IApp>(new AddInProcess(),AddInSecurityLevel.Internet);
                //activate in a new process and set permissions with permissionset
                foreach (var token in tokens)
                {
                    if (!token.Name.Equals(plugin.Config.PluginName)) continue;
                    var app = token.Activate<IDeviceDriverView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
                    var process = new DriverProcess(app, plugin);
                    DeviceDriverDictionary.Add(process.ProcessId, process);
                    app.Initialize(_hostView, process.ProcessId);
                    Console.WriteLine("activated device driver: " + token.Name);
                }
            });
        }

        private bool PluginIsActive(PluginConfig config)
        {
            return DeviceDriverDictionary.Values.Any(driverProcess => driverProcess.Config.Directory.Equals(config.Directory));
        }

        /*public Dictionary<int, ProcessInfo> GetRunningPluginsProcessInfo()
        {
            var collection = new Dictionary<int, ProcessInfo>();
            foreach (var record in DeviceDriverDictionary)
            {
                var counter = record.Value.PerfCounter;
                var processInfo = new ProcessInfo
                {
                    ProcessId = record.Value.ProcessId,
                    Name = record.Value.DeviceDriver.ToString()
                };
                counter.CategoryName = "Process";

                counter.CounterName = "% Processor Time";
                processInfo.PercentProcessorTime = counter.NextValue();

                counter.CounterName = "Private Bytes";
                processInfo.PrivateBytes = (Int64)counter.NextValue();
                collection.Add(record.Key, processInfo);
            }
            return collection;
        }*/
    }
}