﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using CAALHP.Library.Brokers;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Visitor;
using Plugins.Host.HostViews;
using Plugins.Host.LifeCycleManagers;
using Plugins.HostViewAdapters;

namespace Plugins.Host
{
    public class AppHost : Host, IAppHost
    {
        //[DllImport("user32.dll")]
        //static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        //[DllImport("user32.dll")]
        //static extern bool SetForegroundWindow(IntPtr hWnd);

        private readonly IAppHostView _hostView;
        private readonly ICAALHPBroker _broker;
        public AppLifeCycleManager LifeCycleManager { get; private set; }

        public AppHost(EventManager eventManager, ICAALHPBroker broker)
            : base(eventManager)
        {
            _broker = broker;
            _hostView = new AppHostView(this);
            LifeCycleManager = new AppLifeCycleManager(AddInRoot, _hostView);
        }

        public IList<PluginConfig> GetListOfRunningApps()
        {
            return _broker.GetListOfRunningApps();
        }

        public IList<PluginConfig> GetLocalRunningApps()
        {
            return LifeCycleManager.AppDictionary.Select(appProcess => appProcess.Value.Config).ToList();
        }

        public void StartApp(PluginConfig plugin)
        {
            LifeCycleManager.ActivatePlugin(plugin);
        }

        public void CloseApp(int appIndex)
        {
            if (LifeCycleManager.AppDictionary.ContainsKey(appIndex))
            {

                try
                {
                    var appView = LifeCycleManager.AppDictionary[appIndex].App;
                    if (appView == null) return;
                    appView.ShutDown();
                    EventManager.EventSubject.Detach(LifeCycleManager.AppDictionary[appIndex]);
                    var controller = AddInController.GetAddInController(appView);
                    if (controller.AddInEnvironment != null)
                        if (controller.AddInEnvironment.Process != null)
                            controller.AddInEnvironment.Process.Shutdown();
                    if (controller.AppDomain != null)
                        AppDomain.Unload(controller.AppDomain);
                    controller.Shutdown();
                }
                finally
                {
                    LifeCycleManager.AppDictionary.Remove(appIndex);
                }
            }
        }

        public void SwitchToApp(int appIndex)
        {
            //Minimize others (except homebutton)
            //Task.Run(() => MinimizeOthers(appIndex));
            //Show the relevant app
            Task.Run(() => User32.FocusProcess(LifeCycleManager.AppDictionary[appIndex].ProcessId));
            Task.Run(() => LifeCycleManager.AppDictionary[appIndex].App.Show());
            Console.WriteLine(LifeCycleManager.AppDictionary[appIndex].App.GetName());
        }

        /*private void MinimizeOthers(int exception)
        {
            var keys = LifeCycleManager.AppDictionary.Keys.ToList();
            var processId = LifeCycleManager.AppDictionary[exception].ProcessId;
            var proc = Process.GetProcessById(processId);
            var handle = proc.MainWindowHandle;
            //show the exception
            ShowWindow(handle, 3);
            //SetForegroundWindow(handle);

            for (var i = keys.Count - 1; i >= 0; i--)
            {
                //skip if this is the app that should be switched to
                if (keys[i] == exception) continue;
                //skip if this is the topmenu
                var name = AddInController.GetAddInController(LifeCycleManager.AppDictionary[keys[i]].App).Token.Name;
                if (name.ToLower().Contains("topmenu")) continue;
                //hide others
                processId = LifeCycleManager.AppDictionary[keys[i]].ProcessId;
                proc = Process.GetProcessById(processId);
                handle = proc.MainWindowHandle;
                //by doing user32.dll magic
                //2 = SW_SHOWMINIMIZED
                //0 = SW_HIDE
                ShowWindow(handle, 7);
            }
        }*/

        public IList<PluginConfig> GetLocalInstalledApps()
        {
            //read config files
            return GetInstalledPlugins();
        }

        private IList<PluginConfig> GetInstalledPlugins()
        {
            var plugins = new List<PluginConfig>();
            var folders = Directory.EnumerateDirectories(Path.Combine(AddInRoot, "AddIns"));
            foreach (var folder in folders)
            {
                foreach (var file in Directory.GetFiles(folder))
                {
                    var fileName = Path.GetFileName(file);
                    if (fileName == null || !fileName.Equals("CareStorePlugin.xml")) continue;
                    var config = PluginConfigurationManager.GetConfig(file);
                    var plugin = new PluginConfig { Config = config, Directory = Path.GetDirectoryName(file) };
                    plugins.Add(plugin);
                }
            }
            return plugins;
        }


        public override void RegisterPlugin(string name, string category)
        {
            //null implementation
        }

        public override IList<PluginConfig> GetListOfInstalledApps()
        {
            return _broker.GetListOfInstalledApps();
        }


        public override void ShowApp(string appName)
        {
            _broker.ShowApp(appName);
        }

        public async void ShowLocalApp(string appName)
        {
            var apps = GetLocalInstalledApps();
            var foundApp = apps.FirstOrDefault(app => app.Config.PluginName.Equals(appName));
            if (foundApp == null) return;
            await LifeCycleManager.ActivatePlugin(foundApp);
            var copy = LifeCycleManager.AppDictionary.ToList();
            var appIndex = copy.FirstOrDefault(
                x => x.Value.Config.Config.PluginName.Equals(appName))
                .Key;
            if (appIndex != 0)
            {
                //wait for the app to have registered itself
                while (LifeCycleManager.AppDictionary[appIndex].App == null) { Thread.Sleep(100); }
                //then switch to the app
                SwitchToApp(appIndex);
            }
        }

        public override void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void CloseApp(string appName)
        {
            var list = GetListOfRunningApps();
            if (list.All(x => !x.Directory.EndsWith(appName))) return;
            var app = list.First(x => x.Directory.EndsWith(appName));
            if (app == null) return;
            var index = list.IndexOf(app);
            CloseApp(index);
        }

        public override IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            return _broker.GetListOfInstalledDeviceDrivers();
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.AppDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.AppDictionary[processId];
            EventManager.EventSubject.Detach(plugin, fullyQualifiedNameSpace);
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.AppDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.AppDictionary[processId];
            EventManager.EventSubject.Attach(plugin, fullyQualifiedNameSpace);
        }

        public override void ShutDown()
        {
            LifeCycleManager.StopPlugins();
        }
    }
}
