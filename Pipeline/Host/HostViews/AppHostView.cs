﻿using System.Collections.Generic;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;
using Plugins.HostViewAdapters;

namespace Plugins.Host.HostViews
{
    public class AppHostView : IAppHostView
    {
        public IHostBaseView Host { get; set; }
        private readonly IAppHost _appHost;
        
        public AppHostView(IAppHost host)
        {
            _appHost = host;
            Host = new HostView(_appHost);
        }

        public void ShowApp(string appName)
        {
            _appHost.ShowApp(appName);
        }

        public void CloseApp(string appName)
        {
            _appHost.CloseApp(appName);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _appHost.GetListOfInstalledApps().Select(plugin => new PluginInfo { LocationDir = plugin.Directory, Name = plugin.Config.PluginName }).Cast<IPluginInfo>().ToList();
        }
    }
}