﻿using System.Collections.Generic;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;
using Plugins.HostViewAdapters;

namespace Plugins.Host.HostViews
{
    public class ServiceHostView : IServiceHostView
    {
        private readonly IServiceHost _serviceHost;

        public IHostBaseView Host { get; set; }
        
        public ServiceHostView(IServiceHost host)
        {
            _serviceHost = host;
            Host = new HostView(_serviceHost);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _serviceHost.GetListOfInstalledApps().Select(plugin => new PluginInfo { LocationDir = plugin.Directory, Name = plugin.Config.PluginName }).Cast<IPluginInfo>().ToList();
        }

        public IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            return _serviceHost.GetListOfInstalledDeviceDrivers().Select(plugin => new PluginInfo { LocationDir = plugin.Directory, Name = plugin.Config.PluginName }).Cast<IPluginInfo>().ToList();
        }

        public void CloseApp(string fileName)
        {
            _serviceHost.CloseApp(fileName);
        }

        public void ActivateDeviceDrivers()
        {
            _serviceHost.ActivateDrivers();
        }

        public IList<string> GetListOfEventTypes()
        {
            return _serviceHost.GetListOfEventTypes();
        }
    }
}