﻿using System.AddIn.Hosting;
using System.Collections.Generic;
using CAALHP.Events;
using CAALHP.Library.Config;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using Plugins.HostViewAdapters;

namespace Plugins.Host.Processes
{
    public class ServiceProcess : BaseProcess
    {
        public IServiceView Service { get; set; }
        public ServiceProcess(IServiceView service, PluginConfig config) : base(config)
        {
            Service = service;
            ProcessId = AddInController.GetAddInController(service).AddInEnvironment.Process.ProcessId;
            Init();
        }

        public override void Update(KeyValuePair<string, string> theEvent)
        {
            if (Service != null) Service.Notify(theEvent);
        }

        public override void NewEventTypeUpdate(string newFqns)
        {
            var newEvent = new NewEventTypeAddedEvent
            {
                EventType = newFqns,
                CallerName = GetType().FullName,
                //CallerProcessId = Id
            };
            var value = EventHelper.CreateEvent(SerializationType.Json, newEvent);
            Update(value);
        }
    }
}