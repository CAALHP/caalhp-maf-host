﻿using System.Collections.Generic;
using CAALHP.Library.Config;
using CAALHP.Library.Observer;

namespace Plugins.Host.Processes
{
    public abstract class BaseProcess : IEventObserver
    {
        //public Process ProcessReference { get; private set; }
        //public PerformanceCounter PerfCounter { get; private set; }
        public int ProcessId { get; protected set; }
        public int RestartCount { get; set; }
        public PluginConfig Config { get; private set; }

        protected BaseProcess(PluginConfig config)
        {
            //ProcessReference = process;
            //ProcessId = ProcessReference.Id;
            Config = config;
            //Init();
        }

        protected async void Init()
        {
            //await InitPerformanceCounter();
        }

        /*private async Task InitPerformanceCounter()
        {
            await Task.Run(() => { PerfCounter = PerformanceCounterHelper.GetPerformanceCounterByProcessId(ProcessId); });
        }*/

        public abstract void Update(KeyValuePair<string, string> theEvent);
        public abstract void NewEventTypeUpdate(string newFqns);
    }
}