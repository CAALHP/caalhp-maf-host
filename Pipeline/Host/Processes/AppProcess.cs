﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using CAALHP.Library.Config;
using Plugins.HostViewAdapters;

namespace Plugins.Host.Processes
{
    public class AppProcess : BaseProcess
    {
        public IAppView App { get; set; }
        public AppProcess(IAppView app, PluginConfig config) : base(config)
        {
            App = app;
            ProcessId = AddInController.GetAddInController(app).AddInEnvironment.Process.ProcessId;
            Init();
        }
        
        public override void Update(KeyValuePair<string, string> theEvent)
        {
            if (App != null) App.Notify(theEvent);
        }

        public override void NewEventTypeUpdate(string newFqns)
        {
            //throw new NotImplementedException();
        }
    }
}