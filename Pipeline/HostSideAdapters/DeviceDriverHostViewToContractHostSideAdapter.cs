﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    [HostAdapter()]
    public class DeviceDriverHostViewToContractHostSideAdapter : ContractBase, IDeviceDriverHostContract
    {
        public IHostContract Host { get; set; }
        private readonly IDeviceDriverHostView _view;
        
        public DeviceDriverHostViewToContractHostSideAdapter(IDeviceDriverHostView view)
            //: base(view)
        {
            _view = view;
            Host = new HostViewToContractHostSideAdapter(_view.Host);
        }
    }
}