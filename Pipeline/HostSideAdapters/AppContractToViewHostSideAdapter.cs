﻿using System;
using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    // The HostAdapterAttribute identifes this class as the host-side adapter 
    // pipeline segment.
    [HostAdapter()]
    public class AppContractToViewHostSideAdapter : IAppView
    {
        private readonly IAppContract _contract;
        private ContractHandle _handle;

        public AppContractToViewHostSideAdapter(IAppContract contract)
        {
            _contract = contract;
            _handle = new ContractHandle(contract);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            _contract.Notify(notification);
        }

        public bool IsAlive()
        {
            return _contract.IsAlive();
        }

        public void ShutDown()
        {
            try
            {
                _contract.ShutDown();
            }
            catch (Exception e)
            {
                //Happy ending?
                Console.WriteLine(e);
            }
        }

        public void Show()
        {
            _contract.Show();
        }

        public string GetName()
        {
            return _contract.GetName();
        }

        public void Initialize(IAppHostView host, int processId)
        {
            var hostAdapter = new AppHostViewToContractHostSideAdapter(host);
            _contract.Initialize(hostAdapter, processId);
        }
    }
}