﻿using System;
using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    // The HostAdapterAttribute identifes this class as the host-side adapter 
    // pipeline segment.
    [HostAdapter()]
    public class ServiceContractToViewHostSideAdapter : IServiceView
    {
        private readonly IServiceContract _contract;
        private ContractHandle _handle;

        public ServiceContractToViewHostSideAdapter(IServiceContract contract)
        {
            _contract = contract;
            _handle = new ContractHandle(contract);
        }

        public string GetName()
        {
            return _contract.GetName();
        }

        public void Initialize(IServiceHostView host, int processId)
        {
            var hostAdapter = new ServiceHostViewToContractHostSideAdapter(host);
            _contract.Initialize(hostAdapter, processId);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            _contract.Notify(notification);
        }

        public bool IsAlive()
        {
            return _contract.IsAlive();
        }

        public void ShutDown()
        {
            _contract.ShutDown();
        }

        public void Start()
        {
            _contract.Start();
        }

        public void Stop()
        {
            _contract.Stop();
        }
    }
}