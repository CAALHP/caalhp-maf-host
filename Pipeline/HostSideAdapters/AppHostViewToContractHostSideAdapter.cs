using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    [HostAdapter()]
    public class AppHostViewToContractHostSideAdapter : ContractBase, IAppHostContract
    {
        public IHostContract Host { get; set; }
        private readonly IAppHostView _view;
        
        public AppHostViewToContractHostSideAdapter(IAppHostView view) //: base(view)
        {
            _view = view;
            Host = new HostViewToContractHostSideAdapter(_view.Host);
        }
        
        public void ShowApp(string appName)
        {
            _view.ShowApp(appName);
        }

        public void CloseApp(string appName)
        {
            _view.CloseApp(appName);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _view.GetListOfInstalledApps();
        }
    }
}