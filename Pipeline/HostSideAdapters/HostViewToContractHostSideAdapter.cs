﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    /// <summary>
    /// Allows Host side adapter to talk back to HostView
    /// </summary>
    [HostAdapter()]
    public class HostViewToContractHostSideAdapter : ContractBase, IHostContract
    {
        private readonly IHostBaseView _view;

        public HostViewToContractHostSideAdapter(IHostBaseView view)
        {
            _view = view;
        }

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            _view.ReportEvent(value);
        }

        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _view.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
        
        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _view.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}