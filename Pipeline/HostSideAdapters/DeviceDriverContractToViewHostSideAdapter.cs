﻿using System;
using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    // The HostAdapterAttribute identifes this class as the host-side adapter 
    // pipeline segment.
    [HostAdapter()]
    public class DeviceDriverContractToViewHostSideAdapter : IDeviceDriverView
    {
        private readonly IDeviceDriverContract _contract;
        private ContractHandle _handle;

        public DeviceDriverContractToViewHostSideAdapter(IDeviceDriverContract contract)
        {
            _contract = contract;
            _handle = new ContractHandle(contract);
        }

        /*public double GetMeasurement()
        {
            return _contract.GetMeasurement();
        }*/

        public string GetName()
        {
            return _contract.GetName();
        }

        public void Initialize(IDeviceDriverHostView host, int processId)
        {
            var hostAdapter = new DeviceDriverHostViewToContractHostSideAdapter(host);
            _contract.Initialize(hostAdapter, processId);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            _contract.Notify(notification);
        }

        public bool IsAlive()
        {
            return _contract.IsAlive();
        }

        public void ShutDown()
        {
            _contract.ShutDown();
        }
    }
}