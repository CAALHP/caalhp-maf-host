#step1
nuget pack Pipeline\AddInViews\AddInViews.csproj -Symbols
nuget pack Pipeline\AddInViews-Pipeline\AddInViews-Pipeline.csproj -Symbols
nuget pack Pipeline\AddInViews-RefOnly\AddInViews-RefOnly.csproj -Symbols
nuget pack Pipeline\Contracts\Contracts.csproj -Symbols
nuget pack Pipeline\Contracts-Pipeline\Contracts-Pipeline.csproj -Symbols
nuget pack Pipeline\Contracts-RefOnly\Contracts-RefOnly.csproj -Symbols
nuget pack Pipeline\HostViewAdapters\HostViewAdapters.csproj -Symbols
nuget pack Pipeline\HostViewAdapters-Pipeline\HostViewAdapters-Pipeline.csproj -Symbols
nuget pack Pipeline\HostViewAdapters-RefOnly\HostViewAdapters-RefOnly.csproj -Symbols

#step2
nuget pack Pipeline\AddInSideAdapters\AddInSideAdapters.csproj -Symbols
nuget pack Pipeline\Host\Host.csproj -Symbols
nuget pack Pipeline\HostSideAdapters\HostSideAdapters.csproj -Symbols
nuget pack PluginAdapters\PluginAdapter\PluginAdapter.csproj -Symbols

#step3
nuget pack MafHostManager\MafHostManager.csproj -Symbols

#step4
nuget pack AppPluginAdapter\AppPluginAdapter.csproj -Symbols
nuget pack DriverPluginAdapter\DriverPluginAdapter.csproj -Symbols
nuget pack ServicePluginAdapter\ServicePluginAdapter.csproj -Symbols