using System.Linq;
using System.Threading.Tasks;
using CAALHP.Library.Brokers;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Visitor;
using Plugins.Host;

namespace MafHostManager
{
    public class MafHostManager : IHostManager
    {
        public IAppHost AppHost { get; private set; }
        public IDeviceDriverHost DeviceDriverHost { get; private set; }
        public IServiceHost ServiceHost { get; private set; }

        public MafHostManager(EventManager eventManager, ICAALHPBroker broker)
        {
            //start the servicehost first so we have loggingservice ready.
            ServiceHost = new ServiceHost(eventManager, broker);
            DeviceDriverHost = new DeviceDriverHost(eventManager, broker);
            AppHost = new AppHost(eventManager, broker);

            StartSystemApps();
        }

        private async void StartSystemApps()
        {
            await Task.Run(() =>
            {
                var apps = AppHost.GetLocalInstalledApps();
                foreach (var app in apps.Where(app => app.Config.RunAtStartup))
                {
                    AppHost.ShowLocalApp(app.Config.PluginName);
                    //appHost.StartApp(i);
                }
            });
        }

        public void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
            AppHost.Accept(visitor);
            DeviceDriverHost.Accept(visitor);
            ServiceHost.Accept(visitor);
        }
    }
}