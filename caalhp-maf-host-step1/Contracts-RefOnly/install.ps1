﻿param($installPath, $toolsPath, $package, $project)

$file1 = $project.ProjectItems.Item("Pipeline").ProjectItems.Item("Contracts").ProjectItems.Item("CAALHP.Contracts.dll")
$file2 = $project.ProjectItems.Item("Pipeline").ProjectItems.Item("Contracts").ProjectItems.Item("Plugins.Contracts.dll")

# set 'Copy To Output Directory' to 'Copy if newer'
$copyToOutput1 = $file1.Properties.Item("CopyToOutputDirectory")
$copyToOutput1.Value = 2

$copyToOutput2 = $file2.Properties.Item("CopyToOutputDirectory")
$copyToOutput2.Value = 2

$project.Object.References | Where-Object { $_.Name -eq 'Contracts-RefOnly' } | ForEach-Object { Write-Host "removing reference: " $_.Name }
$project.Object.References | Where-Object { $_.Name -eq 'Contracts-RefOnly' } | ForEach-Object { $_.Remove() }

$project.Object.References | Where-Object { $_.Name -eq 'Plugins.Contracts' } | ForEach-Object { Write-Host "setting copy local = false on : " $_.Name }
$project.Object.References | Where-Object { $_.Name -eq 'Plugins.Contracts' } | ForEach-Object { $_.CopyLocal = $false; }