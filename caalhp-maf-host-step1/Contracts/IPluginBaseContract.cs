﻿using System.AddIn.Contract;
using System.AddIn.Pipeline;
using System.Collections.Generic;

namespace Plugins.Contracts
{
    [AddInContract]
    public interface IPluginBaseContract : IContract
    {
        string GetName();
        void Notify(KeyValuePair<string, string> notification);
        bool IsAlive();
        void ShutDown();
    }
}