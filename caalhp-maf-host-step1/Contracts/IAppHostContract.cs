﻿using System.AddIn.Contract;
using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;

namespace Plugins.Contracts
{
    [AddInContract]
    public interface IAppHostContract : IContract
    {
        IHostContract Host { get; set; }
        void ShowApp(string appName);
        void CloseApp(string appName);
        IList<IPluginInfo> GetListOfInstalledApps();
    }
}