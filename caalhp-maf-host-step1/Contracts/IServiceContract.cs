﻿using System.AddIn.Pipeline;

namespace Plugins.Contracts
{
    // The AddInContractAttribute identifes this pipeline segment as a  
    // contract.
    [AddInContract]
    public interface IServiceContract : IPluginBaseContract
    {
        void Start();
        void Stop();
        void Initialize(IServiceHostContract host, int processId);
    }
}
