﻿using System.AddIn.Contract;
using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;

namespace Plugins.Contracts
{
    [AddInContract]
    public interface IServiceHostContract : IContract
    {
        IHostContract Host { get; set; }
        IList<IPluginInfo> GetListOfInstalledApps();
        IList<IPluginInfo> GetListOfInstalledDeviceDrivers();
        void CloseApp(string fileName);
        void ActivateDeviceDrivers();
        IList<string> GetListOfEventTypes();
    }
}