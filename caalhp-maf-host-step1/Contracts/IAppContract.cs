﻿using System.AddIn.Pipeline;

namespace Plugins.Contracts
{
    // The AddInContractAttribute identifes this pipeline segment as a  
    // contract.
    [AddInContract]
    public interface IAppContract : IPluginBaseContract
    {
        void Show();
        void Initialize(IAppHostContract host, int processId);
    }
}
