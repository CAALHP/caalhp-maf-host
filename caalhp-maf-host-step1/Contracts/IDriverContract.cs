﻿using System.AddIn.Pipeline;

namespace Plugins.Contracts
{
    // The AddInContractAttribute identifes this pipeline segment as a  
    // contract.
    [AddInContract]
    public interface IDeviceDriverContract : IPluginBaseContract
    {
        //double GetMeasurement();
        void Initialize(IDeviceDriverHostContract host, int processId);
    }
}
