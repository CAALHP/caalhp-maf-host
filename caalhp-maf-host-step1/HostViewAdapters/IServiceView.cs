﻿namespace Plugins.HostViewAdapters
{
    public interface IServiceView : IPluginBaseView
    {
        void Start();
        void Stop();
        void Initialize(IServiceHostView host, int processId);
    }
}
