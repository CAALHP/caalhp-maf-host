﻿using System.Collections.Generic;

namespace Plugins.HostViewAdapters
{
    public interface IPluginBaseView
    {
        string GetName();
        void Notify(KeyValuePair<string, string> notification);
        bool IsAlive();
        void ShutDown();
    }
}