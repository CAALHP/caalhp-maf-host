﻿namespace Plugins.HostViewAdapters
{
    public interface IAppView : IPluginBaseView
    {
        void Show();
        void Initialize(IAppHostView host, int processId);
    }
}
