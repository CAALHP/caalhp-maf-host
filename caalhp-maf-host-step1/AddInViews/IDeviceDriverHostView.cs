﻿using System.AddIn.Pipeline;

namespace Plugins.AddInViews
{
    [AddInBase()]
    public interface IDeviceDriverHostView
    {
        IHostView Host { get; set; }
    }
}