﻿using System.AddIn.Pipeline;

namespace Plugins.AddInViews
{
    // The AddInBaseAttribute identifes this interface as the basis for 
    // the add-in view pipeline segment.
    [AddInBase()]
    public interface IAppView : IPluginBaseView
    {
        void Initialize(IAppHostView hostView, int processId);
        void Show();
    }
}
