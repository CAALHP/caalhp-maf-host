﻿using System.AddIn.Pipeline;

namespace Plugins.AddInViews
{
    // The AddInBaseAttribute identifes this interface as the basis for 
    // the add-in view pipeline segment.
    [AddInBase()]
    public interface IDeviceDriverView : IPluginBaseView
    {
        void Initialize(IDeviceDriverHostView hostObj, int processId); 
        //double GetMeasurement();
    }
}
