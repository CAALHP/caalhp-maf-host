﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;

namespace Plugins.AddInViews
{
    [AddInBase()]
    public interface IServiceHostView
    {
        IHostView Host { get; set; }
        IList<IPluginInfo> GetListOfInstalledApps();
        IList<IPluginInfo> GetListOfInstalledDeviceDrivers();
        void CloseApp(string fileName);
        void ActivateDeviceDrivers();
        IList<string> GetListOfEventTypes();
    }
}